## Set up environment
1. install Anaconda
2. open anaconda prompt
3. cd to the csvmerge directory
4. run `conda env create -f environment.yml`
5. to test run `conda activate csvmerge`

## Run script
The script support various command line arguments, most of them are optional. The required one is `-p` which is the folder path of the csv files we want to merge.
### Basic usage
1. open anaconda prompt
2. run `conda activate csvmerge`, so python has the required dependencies
3. cd to csvmerge directory
4. run `python csvmerge.py -p C:\path\to\csvfiles`


### more options
run `python csvmerge.py -h` to see them explained
```
$ python csvmerge.py -h
usage: csvmerge.py [-h] -p PATH [-i INPUTSEP] [-o OUTPUTSEP] [-n NAME]

This script merges multiple csv files in a folder into a single one. The input
csv files have to be in specific format: 1st column contains names, 2nd column
the values, 3rd column contains the unit. A new entry is expected in the 6th,
12th, 18th,.... row. Note that only the 2nd column will be processed. In the
output csv additionally the the mean and std are added as the bottom two rows
for each file.

optional arguments:
  -h, --help            show this help message and exit
  -p PATH, --path PATH  path to folder containing the csv files
  -i INPUTSEP, --inputsep INPUTSEP
                        separator for input csv files, default ';'
  -o OUTPUTSEP, --outputsep OUTPUTSEP
                        separator for output csv files, default ';'
  -n NAME, --name NAME  output filename or filepath, default: in current
                        directory named 'merged.csv'

```