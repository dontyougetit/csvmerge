import pandas as pd
import numpy as np
import glob
import os
import argparse

if __name__ == "__main__":

    #data parameters
    inforows = 2  # last entries will be mean and std

    datadesc = ["? Integral", "normalized", "Onset", "Peak", "Endset", "Heating Rate"]
    infodesc = ["Date", "Maschine", "Manufacturer", "Identifier", "Sample", "Run", "Heating Rate", "Type"]
    outcolnames = infodesc + datadesc

    numcols = len(datadesc)  # number of columns of data => number of values a single entry contains

    # this dataframe will contain all data, it will be written out to a csv file
    dfout = pd.DataFrame(columns=outcolnames)

    ########################################################
    # commandline arguments for argparse
    ########################################################
    scriptDescription = "This script merges multiple csv files in a folder into a single one. The input csv files have to be in specific format: 1st column contains names, 2nd column the values, 3rd column contains the unit. A new entry is expected in the {0}th, {1}th, {2}th,.... row. Note that only the 2nd column will be processed. In the output csv additionally the the mean and std are added as the bottom two rows for each file.".format(numcols, numcols*2,numcols*3)

    parser = argparse.ArgumentParser(description=scriptDescription)

    parser.add_argument('-p', '--path',
                        help="path to folder containing the csv files",
                        required=True)
    parser.add_argument('-i', '--inputsep',
                        help="separator for input csv files, default ';'",
                        default=";")
    parser.add_argument('-o', '--outputsep',
                        help="separator for output csv files, default ';'",
                        default=";")
    parser.add_argument("-n","--name",
                        help="output filename or filepath, default: in current directory named 'merged.csv'",
                        default="merged.csv")

    # get cmd args
    args = parser.parse_args()

    path = args.path
    inputsep = args.inputsep
    outputsep = args.outputsep
    searchpattern = os.path.join(path, "*.csv")  # we're looking for csv files

    outputfilename = args.name

    idx = 0
    # iterate over all csv files in path
    for filepath in glob.glob(searchpattern):

        # read in the csv files
        df = pd.read_csv(filepath,
                         sep=inputsep,
                         encoding="ISO-8859-1",  # so special char can be parsed
                         header=None)

        # avoid errors
        if len(df) % numcols != 0:
            print("{0} entries in {1}, expected a length that is divisible by {2}, got a length of {3}, therefore skipping this file".format(
                len(df), filepath, numcols, len(df)))
            continue

        #dynamically create dataframe depending on the number of entries that were found in the file
        numrows = len(df) // numcols #usually equal to 4, because there are 4 entries in each file, but this script supports less or more entries, it just has to be divisible by numcols (currently equal to 6)
        totalrows = numrows + inforows

        # create temporary cleaned up dataframe
        dfclean = pd.DataFrame(index=np.arange(0, totalrows), columns=datadesc)
        dfclean.head()

        # rearrange data, so we have a normal table with datadesc as header
        for i in np.arange(0, len(df), numcols):
            dfclean.iloc[i // numcols] = df.iloc[i:i + numcols, 1].values

        # calculate mean, std and append them
        dfclean.iloc[-2] = dfclean.head(numrows).mean().values
        dfclean.iloc[-1] = dfclean.head(numrows).std().values

        # gather info
        info = os.path.basename(filepath).split("_")
        if len(info) != len(infodesc):
            print(
                "the filename {0} splitted with '_' doesn't yield 8 values, appending empty strings or discarding the last values".format(
                    "_".join(info)))
            if len(info) > len(infodesc):
                info = info[:8]

        # add 6 NaN rows so we can set them accordingly afterwards
        for el in range(totalrows):
            s = pd.Series([np.nan for x in range(len(outcolnames))], index=outcolnames)
            dfout = dfout.append(s, ignore_index=True)

        # set info, inferred from filename
        dfout.loc[idx, 0:len(info)] = info

        # set data
        dfout.loc[idx:idx + totalrows, len(infodesc):len(infodesc) + len(datadesc)] = dfclean.values
        idx += totalrows

    if idx == 0:
        print("no csv files in the directory: {0}".format(path))
    else:
        # write merged dataframe to file
        dfout.to_csv(outputfilename, sep=outputsep, index=False)